# Neo App

"**NeoWs (Near Earth Object Web Service)** is a RESTful web service for near earth Asteroid information. With NeoWs a user can: search for Asteroids based on their closest approach date to Earth, lookup a specific Asteroid with its NASA JPL small body id, as well as browse the overall data-set." Obtained from https://api.nasa.gov/

Icon app generated with Dall-E 2: </br>
![Icon app generated with Dall-E 2](/app/src/main/res/drawable/neoapp.png)

Screenshot profile:

<img src="/screenshots/Profile.png" alt= “ProfileView” width="300" height="600">

Mini Demo [Here](/screenshots/VideoTest.webm)!

## Request From API
**Neo - Feed**
Retrieve a list of Asteroids based on their closest approach date to Earth.
Parameter |Type  | Description
-- | --  |--
start_date | YYYY-MM-DD | Starting date for asteroid search
api_key | API_KEY | api.nasa.gov key for expanded usage

Where start_date is set by default to the date of the interview: 2023-04-26

## Features
- Create an instructional carousel of the first steps with the app
- Implement edit my profile
- Refactor code such as: Empty or unused classes, unused imports
- Add loading animations
- Fix Encrypt & Decrypt password
- Create @string variables in layouts xml

## Code Details

The app allows a user to:
1. View the asteroids of the next 7 days that are closest to Earth from the date defined above
2. View the asteroid detail by its Neo reference ID
3. Get the list of asteroids that the user has saved.
4. View user profile

## Dependencies used
Android application developed with the Java programming language using different dependencies mentioned below:
| Plugin | Version |
| ------ | ------ |
| Retrofit2  | v2.5.1 |
| Room | v2.9.0 |
| Gson | v2.10.1 |
| okhttp | v4.9.0 |
## Requirements

- [x] Minimum version: Android 6 - API level 23

### Made by Jhoser Pacheco
#### jhoserpacheco@gmail.com - www.linkedin.com/in/jhoserpacheco/

