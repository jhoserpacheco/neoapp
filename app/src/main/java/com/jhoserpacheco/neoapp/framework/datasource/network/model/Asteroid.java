package com.jhoserpacheco.neoapp.framework.datasource.network.model;

import java.util.List;

public class Asteroid {

    private String id;
    private String name;
    private String nasaJplUrl;
    private double absoluteMagnitude;
    private EstimatedDiameter estimatedDiameter;
    private boolean isPotentiallyHazardous;
    //private List<CloseApproachData> closeApproachData;
    private String orbitingBody;
}
