package com.jhoserpacheco.neoapp.framework.datasource.network;

import com.jhoserpacheco.neoapp.framework.datasource.network.model.NeoNetwork;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AsteroidApi {
    @GET("feed")
    Call<NeoNetwork> getAsteroids(
            @Query("start_date") String startDate,
            @Query("api_key") String apiKey
    );
/*
    @GET("neo/rest/v1/neo/{id}?api_key="+API_KEY)
    Call<NeoNetwork.NearEarthObject> getAsteroid(@Path("id") String id);
*/
    static final String DATA_INTERVIEW = "2023-04-26";
    static final String API_KEY ="K7paHJPdHaM0gLC7QAhwI3r002Zq5h0HPuXvvJTs";

}
