package com.jhoserpacheco.neoapp.framework.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserWithAsteroid;
import com.jhoserpacheco.neoapp.framework.model.Asteroid;

import java.util.List;

@Dao
public interface AsteroidDao {

    @Query("SELECT * FROM asteroid")
    List<AsteroidEntity> getAll();

    @Transaction
    @Query("SELECT * FROM asteroid WHERE user_id = :userId")
    List<AsteroidEntity> getAllAsteroidByUserId(int userId);

    @Transaction
    @Query("SELECT * FROM asteroid WHERE user_id = :userId")
    AsteroidEntity getAsteroidByUserId(int userId);

    @Query("SELECT * FROM asteroid WHERE id=:id")
    AsteroidEntity getAsteroidById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAsteroid(AsteroidEntity asteroid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAsteroid(List<AsteroidEntity> listAsteroid);

    @Insert
    void saveAllAsteroid(List<AsteroidEntity> asteroid);

    @Update
    void updateAsteroid(AsteroidEntity asteroid);

    @Delete
    void deleteAsteroid(AsteroidEntity asteroid);

}
