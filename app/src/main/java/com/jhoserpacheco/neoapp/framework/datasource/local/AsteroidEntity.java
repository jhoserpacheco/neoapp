package com.jhoserpacheco.neoapp.framework.datasource.local;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "asteroid")
public class AsteroidEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "is_hazardous")
    private Boolean hazardous;
    @ColumnInfo(name = "estimated_diameter")
    private double estimatedDiameter;
    @ColumnInfo(name = "relative_velocity")
    private double relativeVelocity;

    @ColumnInfo(name="close_approach_date")
    private String closeApproachDate;

    @ColumnInfo(name = "user_id")
    private int userId;

    public AsteroidEntity(){}

    public AsteroidEntity(int id, String name, Boolean hazardous, double estimatedDiameter, double relativeVelocity, String closeApproachDate) {
        this.id = id;
        this.name = name;
        this.hazardous = hazardous;
        this.estimatedDiameter = estimatedDiameter;
        this.relativeVelocity = relativeVelocity;
        this.closeApproachDate = closeApproachDate;
    }

    public String getCloseApproachDate() {
        return closeApproachDate;
    }

    public void setCloseApproachDate(String closeApproachDate) {
        this.closeApproachDate = closeApproachDate;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHazardous() {
        return hazardous;
    }

    public void setHazardous(Boolean hazardous) {
        this.hazardous = hazardous;
    }

    public double getEstimatedDiameter() {
        return estimatedDiameter;
    }

    public void setEstimatedDiameter(double estimatedDiameter) {
        this.estimatedDiameter = estimatedDiameter;
    }

    public double getRelativeVelocity() {
        return relativeVelocity;
    }

    public void setRelativeVelocity(double relativeVelocity) {
        this.relativeVelocity = relativeVelocity;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
