package com.jhoserpacheco.neoapp.framework.datasource.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class NeoNetwork {
    @SerializedName("near_earth_objects")
    public HashMap<String, List<AsteroidNetwork>> nearEarthObjects;
    public class CloseApproachData {
        @SerializedName("close_approach_date")
        String closeApproachDate;
        @SerializedName("miss_distance")
        MissDistance missDistance;

        @SerializedName("relative_velocity")
        RelativeVelocity relativeVelocity;

        public String getCloseApproachDate() {
            return closeApproachDate;
        }

        public void setCloseApproachDate(String closeApproachDate) {
            this.closeApproachDate = closeApproachDate;
        }

        public MissDistance getMissDistance() {
            return missDistance;
        }

        public void setMissDistance(MissDistance missDistance) {
            this.missDistance = missDistance;
        }

        public RelativeVelocity getRelativeVelocity() {
            return relativeVelocity;
        }

        public void setRelativeVelocity(RelativeVelocity relativeVelocity) {
            this.relativeVelocity = relativeVelocity;
        }
    }

    public static class AsteroidNetwork {
        @SerializedName("id")
        String id;
        @SerializedName("name")
        String name;
        @SerializedName("is_potentially_hazardous_asteroid")
        boolean hazardous;
        @SerializedName("close_approach_data")
        List<CloseApproachData> closeApproachData;

        public double closestAstronomicalDistance() {
            return closeApproachData.get(0).missDistance.astronomical;
        }
        public double relativeKmSVelocity(){
            return closeApproachData.get(0).relativeVelocity.kilometerPerSecond;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isHazardous() {
            return hazardous;
        }

        public void setHazardous(boolean hazardous) {
            this.hazardous = hazardous;
        }

        public List<CloseApproachData> getCloseApproachData() {
            return closeApproachData;
        }

        public void setCloseApproachData(List<CloseApproachData> closeApproachData) {
            this.closeApproachData = closeApproachData;
        }
    }
    public class MissDistance {
        @SerializedName("astronomical")
        public double astronomical;
    }
    public class RelativeVelocity{
        @SerializedName("kilometers_per_second")
       public double kilometerPerSecond;
    }



}
