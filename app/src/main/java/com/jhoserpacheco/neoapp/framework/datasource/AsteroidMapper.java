package com.jhoserpacheco.neoapp.framework.datasource;

import androidx.annotation.NonNull;

import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;
import com.jhoserpacheco.neoapp.framework.datasource.network.model.NeoNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsteroidMapper {
    public List<AsteroidEntity> fromResponseToEntity(HashMap<String, List<NeoNetwork.AsteroidNetwork>> mapNetwork) {
        List<AsteroidEntity> asteroidEntityList = new ArrayList<>();
        for (Map.Entry<String, List<NeoNetwork.AsteroidNetwork>> entry : mapNetwork.entrySet()) {
            for (NeoNetwork.AsteroidNetwork asteroid : entry.getValue()) {
                AsteroidEntity asteroidEntity = new AsteroidEntity(
                        Integer.parseInt(asteroid.getId()),
                        asteroid.getName(),
                        asteroid.isHazardous(),
                        asteroid.getCloseApproachData().get(0).getMissDistance().astronomical,
                        asteroid.getCloseApproachData().get(0).getRelativeVelocity().kilometerPerSecond,
                        asteroid.getCloseApproachData().get(0).getCloseApproachDate()
                );
                asteroidEntityList.add(asteroidEntity);
            }
        }
        return asteroidEntityList;
    }
}
