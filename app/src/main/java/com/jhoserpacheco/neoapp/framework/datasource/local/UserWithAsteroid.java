package com.jhoserpacheco.neoapp.framework.datasource.local;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class UserWithAsteroid {

    @Embedded public UserEntity user;
    @Relation(parentColumn = "id", entityColumn = "user_id")
    public List<AsteroidEntity> asteroids;

}
