package com.jhoserpacheco.neoapp.framework.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.jhoserpacheco.neoapp.framework.datasource.local.UserEntity;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserWithAsteroid;
import com.jhoserpacheco.neoapp.framework.model.User;

import java.util.List;

@Dao
public interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveUser(UserEntity user);

    @Update
    void updateUser(UserEntity user);

    @Query("SELECT * FROM user WHERE id =:id")
    UserEntity getUserById(int id);

    @Query("SELECT email, encrypted_password FROM user WHERE email = :email AND encrypted_password=:password")
    boolean validateSignIn(String email, String password);

    @Query("SELECT * FROM user WHERE email=:email")
    UserEntity getUserByEmail(String email);

    @Query("SELECT * FROM user WHERE email=:email")
    boolean existUser(String email);

    @Query("SELECT * FROM user")
    List<UserEntity> getAllUser();

    @Transaction
    @Query("SELECT * FROM user WHERE id = :userId")
    List<UserWithAsteroid> getUserWithAsteroid(int userId);



}
