package com.jhoserpacheco.neoapp.framework.model;

public class Asteroid {
    private int id;
    private String name;
    private Boolean hazardous;
    private double estimatedDiameter;
    private double relativeVelocity;
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHazardous() {
        return hazardous;
    }

    public void setHazardous(Boolean hazardous) {
        this.hazardous = hazardous;
    }

    public double getEstimatedDiameter() {
        return estimatedDiameter;
    }

    public void setEstimatedDiameter(double estimatedDiameter) {
        this.estimatedDiameter = estimatedDiameter;
    }

    public double getRelativeVelocity() {
        return relativeVelocity;
    }

    public void setRelativeVelocity(double relativeVelocity) {
        this.relativeVelocity = relativeVelocity;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
