package com.jhoserpacheco.neoapp.framework.datasource.network.model;

import java.util.Map;

public class EstimatedDiameter {

    private Map<String, Double> kilometers;
    private Map<String, Double> meters;
    private Map<String, Double> miles;
    private Map<String, Double> feet;

}
