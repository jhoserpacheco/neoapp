package com.jhoserpacheco.neoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.datasource.AsteroidMapper;
import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;
import com.jhoserpacheco.neoapp.framework.datasource.network.ApiAdapter;
import com.jhoserpacheco.neoapp.framework.datasource.network.AsteroidApi;
import com.jhoserpacheco.neoapp.framework.datasource.network.model.NeoNetwork;
import com.jhoserpacheco.neoapp.ui.AsteroidAdapter;
import com.jhoserpacheco.neoapp.ui.activities.AsteroidDetailActivity;
import com.jhoserpacheco.neoapp.ui.OnItemClickListener;
import com.jhoserpacheco.neoapp.ui.activities.LoginActivity;
import com.jhoserpacheco.neoapp.ui.activities.MyAsteroidActivity;
import com.jhoserpacheco.neoapp.ui.activities.ProfileActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements OnItemClickListener {

    private AppDatabase db;
    private List<AsteroidEntity> asteroidEntities;
    private RecyclerView recyclerView;
    private AsteroidAdapter adapter;
    private Button profile;
    private Button myAsteroids;

    public void init(){
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.listRecyclerViewAsteroid);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getAsteroidsFromApi();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkInternetConnection();
        db = AppDatabase.getAppDatabase(this);
        init();
        goProfile();
        goMyAsteroids();
    }
    public void checkInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No internet connection");
            builder.setMessage("Please check your internet connection and try again.");
            builder.setCancelable(false);
            builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public void getAsteroidsFromApi(){
        AsteroidApi apiService = ApiAdapter.getRetroClient();
        Call<NeoNetwork> call = apiService.getAsteroids(
                "2023-04-30",
                "K7paHJPdHaM0gLC7QAhwI3r002Zq5h0HPuXvvJTs"
        );
        call.enqueue(new Callback<NeoNetwork>() {
            @Override
            public void onResponse(@NonNull Call<NeoNetwork> call, @NonNull Response<NeoNetwork> response) {
                if(response.isSuccessful()) {
                    AsteroidMapper mapper = new AsteroidMapper();
                    assert response.body() != null;
                    asteroidEntities = mapper.fromResponseToEntity(response.body().nearEarthObjects);
                    if(db.asteroidDao().getAll() == null){
                        db.asteroidDao().saveAsteroid(asteroidEntities);
                    }
                    adapter = new AsteroidAdapter(asteroidEntities,getApplicationContext(), MainActivity.this);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<NeoNetwork> call, Throwable t) {
                Toast.makeText(MainActivity.this, "ERROR DE CONEXION", Toast.LENGTH_SHORT);
            }
        });
    }
    //click asteroid item
    @Override
    public void onItemClick(int pos) {
        Intent intent = new Intent(MainActivity.this, AsteroidDetailActivity.class);
        intent.putExtra("ID_ASTEROID", asteroidEntities.get(pos).getId());
        intent.putExtra("NAME", asteroidEntities.get(pos).getName());
        intent.putExtra("HAZARDOUS", asteroidEntities.get(pos).getHazardous());
        intent.putExtra("VELOCITY", asteroidEntities.get(pos).getRelativeVelocity());
        intent.putExtra("DIAMETER", asteroidEntities.get(pos).getEstimatedDiameter());
        intent.putExtra("DATE", asteroidEntities.get(pos).getCloseApproachDate());
        Bundle data = getIntent().getExtras();
        if (data != null){
            if(data.getString("USER_ID") != null){
                intent.putExtra("USER_ID", getIntent().getExtras().getString("USER_ID"));
                Log.d("logDetail", getIntent().getExtras().getString("USER_ID"));
            }
        }
        startActivity(intent);


    }

    //button profile
    public void goProfile(){
        profile = findViewById(R.id.profile_btn);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle data = getIntent().getExtras();
                if (data != null){
                    if(data.getString("USER_ID") != null){
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra("USER_ID", getIntent().getExtras().getString("USER_ID"));
                        startActivity(profileIntent);
                    }
                }else{
                    Toast.makeText(MainActivity.this, "User is not logged", Toast.LENGTH_SHORT).show();
                    Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                }
            }
        });
    }

    public void goMyAsteroids(){
        myAsteroids = findViewById(R.id.my_asteroids);
        myAsteroids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle data = getIntent().getExtras();
                if (data != null){
                    if(data.getString("USER_ID") != null){
                        Intent profileIntent = new Intent(MainActivity.this, MyAsteroidActivity.class);
                        profileIntent.putExtra("USER_ID", getIntent().getExtras().getString("USER_ID"));
                        startActivity(profileIntent);
                    }
                }else{
                    Toast.makeText(MainActivity.this, "User is not logged", Toast.LENGTH_SHORT).show();
                    Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                }
            }
        });
    }

}