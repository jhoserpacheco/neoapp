package com.jhoserpacheco.neoapp.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.jhoserpacheco.neoapp.framework.dao.AsteroidDao;
import com.jhoserpacheco.neoapp.framework.dao.UserDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserEntity;

    @Database(entities = {UserEntity.class, AsteroidEntity.class}, version = 1, exportSchema = false)
    public abstract class AppDatabase extends RoomDatabase {

        private static AppDatabase INSTANCE;

        public abstract UserDao userDao();
        public abstract AsteroidDao asteroidDao();

        public static AppDatabase getAppDatabase(Context context) {
            if (INSTANCE == null) {
                INSTANCE =
                        Room.databaseBuilder(context.getApplicationContext(),
                                        AppDatabase.class, "neows.db")
                                .allowMainThreadQueries()
                                .build();
            }
            return INSTANCE;
        }

        public static void destroyInstance() {
            INSTANCE = null;
        }
    }


