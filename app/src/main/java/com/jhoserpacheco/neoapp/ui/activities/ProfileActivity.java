package com.jhoserpacheco.neoapp.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jhoserpacheco.neoapp.R;
import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.dao.UserDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserEntity;

public class ProfileActivity extends AppCompatActivity {

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        loadDataProfile();
        Button logOutButton = findViewById(R.id.logout_button);
        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                Toast.makeText(ProfileActivity.this, "Logout is successful.", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }

    public void loadDataProfile(){
        db = AppDatabase.getAppDatabase(getApplicationContext());
        UserDao userDao = db.userDao();
        int userId = Integer.parseInt(getIntent().getExtras().getString("USER_ID"));
        UserEntity user = userDao.getUserById(userId);

        TextView email = findViewById(R.id.profile_email);
        TextView firstName = findViewById(R.id.profile_firstname);
        TextView lastName = findViewById(R.id.profile_lastname);
        TextView identification = findViewById(R.id.profile_identification);

        email.setText(user.getEmail());
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        identification.setText(user.getIdentification());

    }
}