package com.jhoserpacheco.neoapp.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jhoserpacheco.neoapp.R;
import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.dao.AsteroidDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;

import java.util.List;

public class AsteroidAdapterOnLong  extends RecyclerView.Adapter<AsteroidAdapterOnLong.ViewHolder>{
    private List<AsteroidEntity> mData;
    private Context context;
    private OnLongClickListener listenerOnClickOnLong;


    public AsteroidAdapterOnLong(List<AsteroidEntity> mData,  Context context, OnLongClickListener listenerOnClickOnLong) {
        this.mData = mData;
        this.context = context;
        this.listenerOnClickOnLong = listenerOnClickOnLong;
    }

    @NonNull
    @Override
    public AsteroidAdapterOnLong.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asteroid,parent,false);
        return new AsteroidAdapterOnLong.ViewHolder(view, listenerOnClickOnLong);
    }

    @Override
    public void onBindViewHolder(@NonNull AsteroidAdapterOnLong.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.name_textview.setText(mData.get(position).getName());
        AsteroidDao asteroidDao = AppDatabase.getAppDatabase(context.getApplicationContext()).asteroidDao();
        CardView card_view = holder.itemView.findViewById(R.id.card_view);
        if (mData.get(position).getHazardous()) {
            card_view.setCardBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
        } else {
            card_view.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green));
        }
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // Eliminar el elemento seleccionado
                asteroidDao.deleteAsteroid(mData.get(position));
                mData.remove(position);
                return true;
            }
        });
        holder.bindData(mData.get(position),listenerOnClickOnLong);
    }

    @Override
    public int getItemCount() {
        return (mData == null) ? 0 : mData.size();
    }
    public void setItems(List<AsteroidEntity> items){mData=items;}

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView name_textview;
        TextView date_textview;

        ViewHolder(View itemView, OnLongClickListener listenerOnClickOnLong){
            super(itemView);
            name_textview = itemView.findViewById(R.id.name_textview);
            date_textview = itemView.findViewById(R.id.date_textview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listenerOnClickOnLong != null){
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION){
                            listenerOnClickOnLong.onItemLongClick(pos);
                        }
                    }
                }
            });

        }
        void bindData(final AsteroidEntity item, OnLongClickListener listenerOnClick){
            name_textview.setText(item.getName());
            date_textview.setText(String.valueOf(item.getCloseApproachDate()));
        }
    }
}
