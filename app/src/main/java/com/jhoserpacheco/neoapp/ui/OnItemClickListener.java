package com.jhoserpacheco.neoapp.ui;

import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;

public interface OnItemClickListener {
    void onItemClick(int posiction);
}
