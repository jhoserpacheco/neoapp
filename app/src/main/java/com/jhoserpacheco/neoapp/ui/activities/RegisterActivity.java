package com.jhoserpacheco.neoapp.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jhoserpacheco.neoapp.R;
import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.dao.UserDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserEntity;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserWithAsteroid;
import com.jhoserpacheco.neoapp.util.EncryptedPassword;

import java.util.Date;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    private EditText emailEditText,passwordEditText,identificationEditText, firstNameEditText, lastNameEditText;
    private TextView createAccountText;
    private Button registerButton;
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        db = AppDatabase.getAppDatabase(getApplicationContext());
        UserDao userDao = db.userDao();

        emailEditText = findViewById(R.id.edit_text_email);
        passwordEditText = findViewById(R.id.edit_text_password);
        identificationEditText = findViewById(R.id.edit_text_identification);
        firstNameEditText = findViewById(R.id.edit_text_first_name);
        lastNameEditText = findViewById(R.id.edit_text_last_name);
        registerButton = findViewById(R.id.button_register);
        createAccountText = findViewById(R.id.create_account_text);

        // url to address an existing account without registering
        createAccountText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString().trim();
                String firstName = firstNameEditText.getText().toString().trim();
                String lastName = lastNameEditText.getText().toString().trim();
                String identification = identificationEditText.getText().toString().trim();
                if(email.isEmpty() || password.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || identification.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "All fields must be complete.", Toast.LENGTH_SHORT).show();
                    return;
                }
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if(!email.matches(emailPattern)) {
                    Toast.makeText(RegisterActivity.this, "Enter a valid email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!userDao.existUser(email)){
                    try {
                        userDao.saveUser(new UserEntity(
                                email,
                                password,
                                firstName,
                                lastName,
                                identification,
                                new Date(System.currentTimeMillis()),
                                new Date(System.currentTimeMillis())
                        ));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    // Register complete
                    Toast.makeText(RegisterActivity.this, "User " + firstName + " Created Successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(RegisterActivity.this, "User already exists", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}