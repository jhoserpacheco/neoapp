package com.jhoserpacheco.neoapp.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.jhoserpacheco.neoapp.MainActivity;
import com.jhoserpacheco.neoapp.R;
import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.dao.AsteroidDao;
import com.jhoserpacheco.neoapp.framework.dao.UserDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserEntity;
import com.jhoserpacheco.neoapp.ui.AsteroidAdapter;
import com.jhoserpacheco.neoapp.ui.AsteroidAdapter;
import com.jhoserpacheco.neoapp.ui.OnItemClickListener;
import com.jhoserpacheco.neoapp.ui.OnLongClickListener;

import java.util.List;

public class MyAsteroidActivity extends AppCompatActivity implements OnItemClickListener  {

    private AppDatabase db;
    private AsteroidAdapter adapter;
    private RecyclerView recyclerView;
    List<AsteroidEntity> myAsteroidList;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_asteroid);
        
        init();
    }

    public void init(){
        setContentView(R.layout.my_asteroid);
        recyclerView = findViewById(R.id.myListRecyclerViewAsteroid);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        loadMyAsteroids();
    }
    public void loadMyAsteroids(){
        db = AppDatabase.getAppDatabase(getApplicationContext());
        AsteroidDao asteroidDao = db.asteroidDao();
        int userId = Integer.parseInt(getIntent().getExtras().getString("USER_ID"));
        TextView listEmptyAsteroids = findViewById(R.id.listEmptyAsteroids);
        myAsteroidList = asteroidDao.getAllAsteroidByUserId(userId);
        if(myAsteroidList.size() == 0){
            listEmptyAsteroids.setText("Empty Asteroids");
            listEmptyAsteroids.setTextSize(24);
            listEmptyAsteroids.setPadding(0,720,0,0);
        }

        adapter = new AsteroidAdapter(myAsteroidList,getApplicationContext(),  MyAsteroidActivity.this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(int position) {
        db = AppDatabase.getAppDatabase(getApplicationContext());
        AsteroidDao asteroidDao = db.asteroidDao();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose an option")
                .setPositiveButton("View", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(MyAsteroidActivity.this, AsteroidDetailActivity.class);
                        intent.putExtra("ID_ASTEROID", myAsteroidList.get(position).getId());
                        intent.putExtra("NAME", myAsteroidList.get(position).getName());
                        intent.putExtra("HAZARDOUS", myAsteroidList.get(position).getHazardous());
                        intent.putExtra("VELOCITY", myAsteroidList.get(position).getRelativeVelocity());
                        intent.putExtra("DIAMETER", myAsteroidList.get(position).getEstimatedDiameter());
                        intent.putExtra("DATE", myAsteroidList.get(position).getCloseApproachDate());
                        intent.putExtra("USER_ID", getIntent().getExtras().getString("USER_ID"));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        asteroidDao.deleteAsteroid(myAsteroidList.get(position));
                        myAsteroidList.remove(position);
                        recreate();
                    }
                });
        builder.create().show();
    }

}