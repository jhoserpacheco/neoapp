package com.jhoserpacheco.neoapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jhoserpacheco.neoapp.R;
import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.dao.AsteroidDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.AsteroidEntity;

public class AsteroidDetailActivity extends AppCompatActivity {

    AppDatabase db;
    AsteroidDao asteroidDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asteroid_detail);

        db = AppDatabase.getAppDatabase(this);
        asteroidDao = db.asteroidDao();

        int id = getIntent().getIntExtra("ID_ASTEROID", -1);
        String name = getIntent().getStringExtra("NAME");
        Boolean hazardous = getIntent().getBooleanExtra("HAZARDOUS", false);
        Double velocity = getIntent().getDoubleExtra("VELOCITY", 0);
        Double diameter = getIntent().getDoubleExtra("DIAMETER", 0);
        String date = getIntent().getStringExtra("DATE");

        TextView nameTextView = findViewById(R.id.name_textview);
        TextView velocityTextView = findViewById(R.id.velocity_textview);
        TextView diameterTextView = findViewById(R.id.estimated_diameter);
        TextView dateTextView = findViewById(R.id.date);

        Button saveAsteroidButton = findViewById(R.id.saveAsteroid);

        nameTextView.setText(name);
        velocityTextView.setText(String.valueOf(velocity).concat(" Km/S"));
        diameterTextView.setText(String.valueOf(diameter).concat(" au"));
        dateTextView.setText(date);

        saveAsteroidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int userId = -1;
                if (getIntent().getStringExtra("USER_ID") != null) {
                    userId = Integer.parseInt(getIntent().getStringExtra("USER_ID"));
                    Log.d("before", userId + "");
                }
                Log.d("user_id", userId + "");
                //Usuario no esta logueado
                if (userId == -1) {
                    Toast.makeText(AsteroidDetailActivity.this, "User is not logged in", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AsteroidDetailActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    saveAsteroid(userId,id,hazardous,name,diameter,velocity,date);
                    Toast.makeText(AsteroidDetailActivity.this, "Asteroid saved successfully", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void saveAsteroid(int userId,int id, boolean hazardous, String name, double diameter, double velocity, String date){
        AsteroidEntity asteroid = new AsteroidEntity();
        if(id != -1){
            asteroid.setId(id);
            asteroid.setHazardous(hazardous);
            asteroid.setName(name);
            asteroid.setEstimatedDiameter(diameter);
            asteroid.setRelativeVelocity(velocity);
            asteroid.setCloseApproachDate(date);
        }else{
            asteroid = asteroidDao.getAsteroidById(id);
        }
        asteroid.setUserId(userId);
        asteroidDao.saveAsteroid(asteroid);
    }
}

