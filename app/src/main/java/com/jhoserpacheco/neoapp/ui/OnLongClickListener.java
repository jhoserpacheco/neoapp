package com.jhoserpacheco.neoapp.ui;

public interface OnLongClickListener {
    void onItemLongClick(int position);
}
