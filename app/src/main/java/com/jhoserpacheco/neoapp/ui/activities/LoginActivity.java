package com.jhoserpacheco.neoapp.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jhoserpacheco.neoapp.MainActivity;
import com.jhoserpacheco.neoapp.R;
import com.jhoserpacheco.neoapp.db.AppDatabase;
import com.jhoserpacheco.neoapp.framework.dao.UserDao;
import com.jhoserpacheco.neoapp.framework.datasource.local.UserEntity;
import com.jhoserpacheco.neoapp.util.EncryptedPassword;

public class LoginActivity extends AppCompatActivity {
    private TextView createRegisterAccount;
    private EditText emailLoginEditText,passwordLoginEditText;
    private Button loginButton;
    private AppDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);


        emailLoginEditText = findViewById(R.id.login_email);
        passwordLoginEditText = findViewById(R.id.login_password);
        createRegisterAccount = findViewById(R.id.create_register_account);
        loginButton = findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailLoginEditText.getText().toString().trim();
                String password = passwordLoginEditText.getText().toString().trim();

                if(email.isEmpty() || password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "All fields must be complete.", Toast.LENGTH_SHORT).show();
                    return;
                }
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if(!email.matches(emailPattern)) {
                    Toast.makeText(LoginActivity.this, "Enter a valid email", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    UserEntity user = validateLogin(email);
                    if(user != null){
                        /*
                        Log.d("pass", EncryptedPassword.decrypt(password));
                        Log.d("passUser",user.getEncryptedPassword());
                        Log.d("passLogin", password);*/
                        if(user.getEmail().equals(email) && user.getEncryptedPassword().equals(password)){
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("USER_ID", String.valueOf(user.getId()));
                            Toast.makeText(LoginActivity.this, "Login is successful.", Toast.LENGTH_SHORT).show();
                            startActivity(intent);
                        }else{
                            Toast.makeText(LoginActivity.this, "Email or Password is invalid. ", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(LoginActivity.this, "User not Exist. ", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });



        createRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private UserEntity validateLogin(String email) throws Exception {
        db = AppDatabase.getAppDatabase(getApplicationContext());
        UserDao userDao = db.userDao();
        UserEntity user = userDao.getUserByEmail(email);
        if (user != null){
            return user;
        }
        return null;
    }
}