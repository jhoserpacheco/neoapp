package com.jhoserpacheco.neoapp.util;

import android.util.Base64;

import java.nio.charset.StandardCharsets;

public class EncryptedPassword {

    public static String encrypt(String value) throws Exception {
        byte[] encodedBytes = Base64.encode(value.getBytes(), Base64.DEFAULT);
        return new String(encodedBytes, StandardCharsets.UTF_8).trim();
    }

    public static String decrypt(String value) throws Exception {
        byte[] decodedBytes = Base64.decode(value.getBytes(), Base64.DEFAULT);
        return new String(decodedBytes, StandardCharsets.UTF_8).trim();
    }
}
