package com.jhoserpacheco.neoapp.util;

import androidx.room.TypeConverter;

import java.util.Date;

public class TimestampConverter {
    @TypeConverter
    public static Long fromDate(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static Date toDate(Long time) {
        return time == null ? null : new Date(time);
    }

}

